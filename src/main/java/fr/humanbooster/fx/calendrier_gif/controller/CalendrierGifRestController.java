package fr.humanbooster.fx.calendrier_gif.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.humanbooster.fx.calendrier_gif.business.Emotion;
import fr.humanbooster.fx.calendrier_gif.business.Gif;
import fr.humanbooster.fx.calendrier_gif.business.Jour;
import fr.humanbooster.fx.calendrier_gif.business.Reaction;
import fr.humanbooster.fx.calendrier_gif.business.Theme;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;
import fr.humanbooster.fx.calendrier_gif.service.EmotionService;
import fr.humanbooster.fx.calendrier_gif.service.GifService;
import fr.humanbooster.fx.calendrier_gif.service.JourService;
import fr.humanbooster.fx.calendrier_gif.service.ReactionService;
import fr.humanbooster.fx.calendrier_gif.service.ThemeService;
import fr.humanbooster.fx.calendrier_gif.service.UtilisateurService;

@RestController
@RequestMapping("/")
public class CalendrierGifRestController {
	
	private CalendrierGifController calendrierGifController;
	
	private JourService jourService;
	private EmotionService emotionService;
	private UtilisateurService utilisateurService;
	private ThemeService themeService;
	private GifService gifService;
	private ReactionService reactionService;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");

	
	public CalendrierGifRestController(CalendrierGifController calendrierGifController) {
		super();
		this.calendrierGifController = calendrierGifController;
	}
	
	@PostConstruct
	public void init() {
		this.jourService = calendrierGifController.jourService;
		this.emotionService = calendrierGifController.emotionService;
		this.utilisateurService = calendrierGifController.utilisateurService;
		this.themeService = calendrierGifController.themeService;
		this.gifService = calendrierGifController.gifService;
		this.reactionService = calendrierGifController.reactionService;
		
	}
	
	//Methode A
	@PostMapping(value="/jours/{dateString}", produces="application/json")
	Jour ajouterJour(@PathVariable String dateString) {
		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (date != null) {
			Jour jour = jourService.ajouterJour(date);
			return jour;
		}
		return null;
	}
	
	//Methode B
	@PostMapping(value="/utilisateurs/{nom}/{prenom}/{email}/{motDePasse}/{themeStr}", produces="application/json")
	Utilisateur ajouterUtilisateur(@PathVariable String nom, @PathVariable String prenom, @PathVariable String email, @PathVariable String motDePasse, @PathVariable String themeStr) {
		Theme theme = themeService.recupererTheme(themeStr);
		Utilisateur utilisateur = utilisateurService.ajouterUtilisateur(nom, prenom, email, motDePasse, theme);
		return utilisateur;
	}
	
	
	//Methode C
	@GetMapping(value="/jours/{dateString}/gif", produces="application/json")
	Gif recupererGifParDate(@PathVariable String dateString) {
		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Jour jour = jourService.recupererJour(date);
		if (jour != null) {
			Gif gif = gifService.recupererGifParDate(jour);		
			return gif;
		}
		return null;
	}
	
	//Methode D
	@PutMapping(value="/jours/{dateStr}/gif/{legendeMaj}", produces="application/json")
	Gif majLegende(@PathVariable String dateStr, @PathVariable String legendeMaj) {
		Gif gif = recupererGifParDate(dateStr);
		if (gif != null) {			
			gifService.mettreAJourLegende(gif, legendeMaj);
			return gif;
		}
		return null;
	}
	
	//Methode E
	@PostMapping(value="/emotion/{nom}/{unicode}", produces="application/json")
	Emotion ajoutEmotion(@PathVariable String nom, @PathVariable String unicode) {
		Emotion emotion = emotionService.ajouterEmotion(nom, unicode);
		return emotion;
	}
	
	//Methode F
	@GetMapping(value="/jours/{dateStr}/reactions", produces="application/json")
	List<Reaction> recupererReactions(@PathVariable String dateStr) {
		Gif gif = recupererGifParDate(dateStr);
		if (gif != null) {			
			return gif.getReactions();
		}
		return null;
	}
	
	//Methode G
	@DeleteMapping(value="delete/{idUtilisateur}/{mail}/{dateStr}", produces="application/json")
	boolean supprimerReaction(@PathVariable Long idUtilisateur, @PathVariable String mail, @PathVariable String dateStr) {
		Gif gif = recupererGifParDate(dateStr);
		Emotion emotion = null;
		for (Reaction reaction : gif.getReactions()) {
			if (reaction.getUtilisateur().equals(utilisateurService.recupererUtilisateur(idUtilisateur))) {
				emotion = reaction.getEmotion();
			}
		}
		if (gif != null) {
			return reactionService.supprimerReaction(gif.getId(), emotion.getId(), utilisateurService.recupererUtilisateur(idUtilisateur).getId(), mail);
		}
		return false;
		
	}
	
	//Methode H
	@GetMapping(value="utilisateurs/{idUtilisateur}/gifs", produces="application/json")
	List<Gif> recupererGifsParUtilisateur(@PathVariable Long idUtilisateur) {
		Utilisateur utilisateur = utilisateurService.recupererUtilisateur(idUtilisateur);
		if (utilisateur != null) {
			return utilisateur.getGifs();
		}
		return null;
	}
	
	//Methode I
	@GetMapping(value="/gifs", produces="application/json")
	List<Gif> recupererGifParLegende(@RequestParam(name="legende") String charSeq) {
		List<Gif> gifs = new ArrayList<>();
		for (Gif gif : gifService.recupererGifs()) {
			if (gif.getLegende().contains(charSeq)) {
				gifs.add(gif);
			}
		}
		return gifs;
	}
	
	//Methode J
	@GetMapping(value="/topGif", produces="application/json")
	Gif recupererGifAvecMaxReactions() {
		int nbReactionMax = 0;
		for (Gif gif : gifService.recupererGifs()) {
			if (gif.getReactions().size() > nbReactionMax) {
				nbReactionMax = gif.getReactions().size();
			}
		}
		
		List<Gif> gifsReactionMax = new ArrayList<>();
		for (Gif gif : gifService.recupererGifs()) {
			if (gif.getReactions().size() == nbReactionMax) {
				gifsReactionMax.add(gif);
			}
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(2020, 3, 30, 0, 0, 0);
		Date dateLaPlusRecente = calendar.getTime();
		String dateRecente =  sdf.format(dateLaPlusRecente);
		Date dateR = null;
		try {
			dateR = sdf.parse(dateRecente);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (Gif gif : gifsReactionMax) {
			System.out.println("getjour" + gif.getJour().getDate());
			System.out.println("dateplus" + dateR);
			if (gif.getJour().getDate().after(dateR)) {
				dateR = gif.getDateHeureAjout();
			}
		}
		
		Gif gif = gifService.recupererGifParDate(jourService.recupererJour(dateR));
		if (gif != null) {
			return gif;
		}
		return null;
	}
	
	
	

}
