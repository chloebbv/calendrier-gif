package fr.humanbooster.fx.calendrier_gif.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.fx.calendrier_gif.business.GifDistant;
import fr.humanbooster.fx.calendrier_gif.business.Jour;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;
import fr.humanbooster.fx.calendrier_gif.service.EmotionService;
import fr.humanbooster.fx.calendrier_gif.service.GifService;
import fr.humanbooster.fx.calendrier_gif.service.JourService;
import fr.humanbooster.fx.calendrier_gif.service.ReactionService;
import fr.humanbooster.fx.calendrier_gif.service.ThemeService;
import fr.humanbooster.fx.calendrier_gif.service.UtilisateurService;

@Controller
@RequestMapping("/")
public class CalendrierGifController {

	private static final int NB_JOURS_PAR_PAGE = 7;
	
	protected JourService jourService;
	protected EmotionService emotionService;
	protected UtilisateurService utilisateurService;
	protected ThemeService themeService;
	protected GifService gifService;
	protected ReactionService reactionService;
	protected HttpSession httpSession;
	protected ServletContext servletContext;

	public CalendrierGifController(JourService jourService, EmotionService emotionService,
			UtilisateurService utilisateurService, ThemeService themeService, GifService gifService,
			ReactionService reactionService, HttpSession httpSession, ServletContext servletContext) {
		super();
		this.jourService = jourService;
		this.emotionService = emotionService;
		this.utilisateurService = utilisateurService;
		this.themeService = themeService;
		this.gifService = gifService;
		this.reactionService = reactionService;
		this.httpSession = httpSession;
		this.servletContext = servletContext;
	}

	@RequestMapping(value = { "/index", "/" })
	public ModelAndView accueil() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		return mav;
	}
	
	@PostMapping("/connexion")
	public ModelAndView connexion(@RequestParam("EMAIL") String email, @RequestParam("MOT_DE_PASSE") String motDePasse) {
		Utilisateur utilisateur = utilisateurService.recupererUtilisateur(email, motDePasse);
		if (utilisateur==null) {
			ModelAndView mav = accueil();
			mav.addObject("notification", "Mot de passe et/ou email incorrect");
			return mav;
		}
		else {
			httpSession.setAttribute("utilisateur", utilisateur);
			ModelAndView mav = new ModelAndView("redirect:calendrier");
			return mav;
		}
	}
	
	@GetMapping("/calendrier")
	public ModelAndView calendrier(@PageableDefault(size = NB_JOURS_PAR_PAGE, sort = "date") Pageable pageable) {
		ModelAndView mav = new ModelAndView("calendrier");
		mav.addObject("pageDeJours", jourService.recupererJours(pageable));
		// Met en session la page choisie
		if (pageable!=null) {
			httpSession.setAttribute("numeroDePage", pageable.getPageNumber());
		}
		return mav;		
	}

	@GetMapping("/placerGifDistant")
	public ModelAndView placerGifDistantGet(@RequestParam("ID_JOUR") Date idJour) {		
		ModelAndView mav = new ModelAndView("placerGifDistant");
		GifDistant gifDistant = new GifDistant();
		Jour jour = jourService.recupererJour(idJour);
		gifDistant.setJour(jour);
		mav.addObject("gifDistant", gifDistant);
		return mav;		
	}

	@PostMapping("/placerGifDistant")
	public ModelAndView placerGifDistantPost(@Valid @ModelAttribute GifDistant gifDistant, BindingResult result) {		
		if (result.hasErrors()) {
			ModelAndView mav = placerGifDistantGet(gifDistant.getJour().getDate());
			mav.addObject("gifDistant", gifDistant);
			return mav;
		}
		else {
			// On vérifie qu'il n'y a pas déja un gif sur ce jour
			if (gifDistant.getJour().getGif()==null) {
				gifService.ajouterGifDistant(gifDistant, (Utilisateur) httpSession.getAttribute("utilisateur"));
			}
			//return new ModelAndView("redirect:calendrier");
			if (httpSession.getAttribute("numeroDePage")!=null)
			{
				return new ModelAndView("redirect:calendrier?page=" + httpSession.getAttribute("numeroDePage"));
			}
			else {
				return new ModelAndView("redirect:calendrier");
			}
		}
	}

	@GetMapping("/reagir")
	public ModelAndView reagirGet(@RequestParam("ID_GIF") Long idGif) {		
		ModelAndView mav = new ModelAndView("reagir");
		mav.addObject("gif", gifService.recupererGif(idGif));
		mav.addObject("emotions", emotionService.recupererEmotions());
		return mav;
	}

	@PostMapping("/reagir")
	public ModelAndView reagirPost(@RequestParam("ID_GIF") Long idGif, @RequestParam("ID_EMOTION") Long idEmotion) {		
		reactionService.ajouterReaction(idGif, idEmotion, (Utilisateur) httpSession.getAttribute("utilisateur"));
		if (httpSession.getAttribute("numeroDePage")!=null)
		{
			return new ModelAndView("redirect:calendrier?page=" + httpSession.getAttribute("numeroDePage"));
		}
		else {
			return new ModelAndView("redirect:calendrier");
		}
	}

	@GetMapping("/deconnexion")
	public ModelAndView deconnexion() {
		httpSession.invalidate();
		ModelAndView mav = accueil();
		mav.addObject("notification", "Au revoir");
		return mav;
	}

	@GetMapping("/inscription")
	public ModelAndView inscriptionGet() {
		ModelAndView mav = new ModelAndView("inscription");
		mav.addObject("themes", themeService.recupererThemes());
		mav.addObject("utilisateur", new Utilisateur());
		return mav;
	}
	
	@PostMapping("/inscription")
	public ModelAndView inscriptionPost(@Valid @ModelAttribute Utilisateur utilisateur, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = inscriptionGet();
			mav.addObject("utilisateur", utilisateur);
			return mav;
		}
		else  {
			utilisateurService.ajouterUtilisateur(utilisateur);
			ModelAndView mav = accueil();
			mav.addObject("notification", "Utilisateur ajouté");
			return mav;
		}
	}
		
	@PostConstruct
	private void init() {
		if (jourService.recupererJours().isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			for (int i = 1; i <= 31; i++) {
				jourService.ajouterJour(calendar.getTime());
				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		
		if (emotionService.recupererEmotions().isEmpty()) {			
			emotionService.ajouterEmotion("Souriant", "&#x1F600;");
			emotionService.ajouterEmotion("Monocle", "&#x1F9D0;");
			emotionService.ajouterEmotion("Bisous", "&#x1F618;");
			emotionService.ajouterEmotion("Coeur", "&#x1F60D;");
			emotionService.ajouterEmotion("PTDR", "&#x1F923;");
		}

		if (themeService.recupererThemes().isEmpty()) {
			themeService.ajouterTheme("Bachata");
			themeService.ajouterTheme("Dark");
		}
		
		if (utilisateurService.recupererUtilisateur("charlene@hb.com", "123")==null) {
			utilisateurService.ajouterUtilisateur("DUCREUX", "Charlène", "charlene@hb.com", "123", themeService.recupererTheme("Dark"));
		}

	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Jour.FORMAT_DATE_AMERICAIN);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(simpleDateFormat, true));
	}
}