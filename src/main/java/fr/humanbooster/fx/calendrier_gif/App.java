package fr.humanbooster.fx.calendrier_gif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@ComponentScan(basePackages = "fr.humanbooster.fx.calendrier_gif")
@EnableJpaRepositories(basePackages = "fr.humanbooster.fx.calendrier_gif", entityManagerFactoryRef = "entityManagerFactory")
public class App {

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}