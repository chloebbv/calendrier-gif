package fr.humanbooster.fx.calendrier_gif.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.calendrier_gif.business.Gif;
import fr.humanbooster.fx.calendrier_gif.business.Jour;

public interface GifDao extends JpaRepository<Gif, Long> {

	Gif findByJour(Jour jour);

}
