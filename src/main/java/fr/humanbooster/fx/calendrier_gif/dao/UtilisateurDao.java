package fr.humanbooster.fx.calendrier_gif.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;

public interface UtilisateurDao extends JpaRepository<Utilisateur, Long> {

	Utilisateur findLastByEmailAndMotDePasse(String email, String motDePasse);

	Utilisateur findByIdAndEmail(Long idUtilisateur, String email);
	
	Utilisateur findByEmail(String email);

}
