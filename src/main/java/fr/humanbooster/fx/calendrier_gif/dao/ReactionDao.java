package fr.humanbooster.fx.calendrier_gif.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.fx.calendrier_gif.business.Emotion;
import fr.humanbooster.fx.calendrier_gif.business.Gif;
import fr.humanbooster.fx.calendrier_gif.business.Reaction;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;

public interface ReactionDao extends JpaRepository<Reaction, Long> {

	List<Reaction> findByGif(Gif gif);

	Reaction findLastByGifAndUtilisateurAndEmotion(Gif recupererGif, Utilisateur utilisateur, Emotion findOne);

}