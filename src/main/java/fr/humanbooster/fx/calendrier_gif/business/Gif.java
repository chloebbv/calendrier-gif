package fr.humanbooster.fx.calendrier_gif.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public abstract class Gif {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	
	protected Date dateHeureAjout;
	
	protected String legende;
	
	@JsonIgnore
	@OneToOne
	protected Jour jour;

	@JsonIgnore
	@ManyToOne
	private Utilisateur utilisateur;

	@JsonIgnore
	@OneToMany(mappedBy="gif", fetch=FetchType.EAGER)
	private List<Reaction> reactions;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateHeureAjout() {
		return dateHeureAjout;
	}

	public void setDateHeureAjout(Date dateHeureAjout) {
		this.dateHeureAjout = dateHeureAjout;
	}

	public String getLegende() {
		return legende;
	}

	public void setLegende(String legende) {
		this.legende = legende;
	}

	public Jour getJour() {
		return jour;
	}

	public void setJour(Jour jour) {
		this.jour = jour;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public List<Reaction> getReactions() {
		return reactions;
	}

	public void setReactions(List<Reaction> reactions) {
		this.reactions = reactions;
	}

	@Override
	public String toString() {
		return "Gif [id=" + id + ", dateHeureAjout=" + dateHeureAjout + ", legende=" + legende + ", jour=" + jour
				+ ", utilisateur=" + utilisateur.getPrenom() + "]";
	}

}