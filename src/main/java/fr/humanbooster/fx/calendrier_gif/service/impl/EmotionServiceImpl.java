package fr.humanbooster.fx.calendrier_gif.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.fx.calendrier_gif.business.Emotion;
import fr.humanbooster.fx.calendrier_gif.dao.EmotionDao;
import fr.humanbooster.fx.calendrier_gif.service.EmotionService;

@Service
public class EmotionServiceImpl implements EmotionService {

	@Autowired
	private EmotionDao emotionDao;
	
	@Override
	public Emotion ajouterEmotion(String nom, String nomFichier) {
		return emotionDao.save(new Emotion(nom, nomFichier));
	}

	@Override
	public List<Emotion> recupererEmotions() {
		return emotionDao.findAll();
	}

	@Override
	public Emotion recupererEmotion(Long id) {
		return emotionDao.findOne(id);
	}

	@Override
	public Emotion recupererEmotion(String nom) {
		return emotionDao.findByNom(nom);
	}

}
