package fr.humanbooster.fx.calendrier_gif.service;

import fr.humanbooster.fx.calendrier_gif.business.Theme;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;

public interface UtilisateurService {

	Utilisateur ajouterUtilisateur(Utilisateur utilisateur);
	
	Utilisateur ajouterUtilisateur(String nom, String prenom, String email, String motDePasse, Theme recupererTheme);

	Utilisateur recupererUtilisateur(String email, String motDePasse);
	
	Utilisateur recupererUtilisateur(String email);

	Utilisateur recupererUtilisateur(Long idUtilisateur);

}
