package fr.humanbooster.fx.calendrier_gif.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.fx.calendrier_gif.business.Gif;
import fr.humanbooster.fx.calendrier_gif.business.GifDistant;
import fr.humanbooster.fx.calendrier_gif.business.Jour;
import fr.humanbooster.fx.calendrier_gif.business.Utilisateur;

public interface GifService {

	GifDistant ajouterGifDistant(Date idJour, String url, Utilisateur utilisateur);

	GifDistant ajouterGifDistant(GifDistant gifDistant, Utilisateur utilisateur);

	Gif recupererGif(Long idGif);

	Gif mettreAJourLegende(Gif gif, String nouvelleLegende);
	
	Gif recupererGifParDate(Jour jour);

	List<Gif> recupererGifs();
	
}
