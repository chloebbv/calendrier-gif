package fr.humanbooster.fx.calendrier_gif.service;

import java.util.List;

import fr.humanbooster.fx.calendrier_gif.business.Emotion;

public interface EmotionService {

	Emotion ajouterEmotion(String nom, String nomFichier);

	List<Emotion> recupererEmotions();
	
	Emotion recupererEmotion(Long id);
	
	Emotion recupererEmotion(String nom);
}
