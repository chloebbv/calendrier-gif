package fr.humanbooster.fx.calendrier_gif.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.fx.calendrier_gif.business.Jour;

public interface JourService {

	Jour ajouterJour(Date date);
	Jour enregistrerJour(Jour jour);
	
	Page<Jour> recupererJours(Pageable pageable);

	List<Jour> recupererJours();

	Jour recupererJour(Date idJour);

	boolean supprimerJour(Date date);
}
